# Game of Life in Terraform

This repo showcases the advanced list comprehension and function functionality in Terraform to implement Conway's Game of Life.

As well as showcasing expressivness of HCL2, it:

* Enables multiple people to collaboratively simulate Game of Life by utilising remote state
* Enables creating cloud resources based on the outcome of Game of Life
* When using a versioned state storage, allows to roll back simulation to any previous step just by replacing the state file

## Demo

[![asciicast](https://asciinema.org/a/DAeA2x8JbBvi3ozqRpxWiRouU.svg)](https://asciinema.org/a/DAeA2x8JbBvi3ozqRpxWiRouU)

## Requirements

* Terraform 0.13+ (incompatible with 0.12 due to a bug in HCL implementation)
* Time (this takes forever to run)

## Usage

To initialise the simulation:

```bash
terraform apply -auto-approve -var new_game=true
```

The code creates no resources and uses no providers, so you do not have to `terraform init` the repository!

You may also chose the initial seed with `seed` variable. Alive cells are represented with a star, and empty cells with a space.

Once the simulation is initialised, to step a single epoch:

```bash
terraform apply -auto-approve
```
